git clone https://gitlab.com/intangiblerealities/narupa-applications/narupa-imd.git
Set-Location -Path narupa-imd
$patch_number = git rev-list --count HEAD
$version_number = "0.1.${patch_number}"
Set-Location -Path ..\
Write-Output $version_number

